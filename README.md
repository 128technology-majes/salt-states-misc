# 128T salt-states-misc

This repo contains additional salt states and modules that can be used along with the 128T Automated provizioner for full Zero Touch Provisioning of new 128T rotuers. These states can also be used to automate specific Linux functions on existing 128T routers.

The base repo can be found here: https://github.com/128technology/salt-states

## Installation

```
$ dnf install -y git
$ cd /usr/local/src
$ git clone git@github.com:128technology/salt-states.git
$ git clone git@gitlab.com:128technology-majes/salt-states-misc.git
$ printf '\n%-24s%-24s%-8s%s 0 0\n' overlay /srv/salt overlay \
lowerdir=/usr/local/src/salt-states-misc:/usr/local/src/salt-states,upperdir=/srv/salt,workdir=/tmp \
>> /etc/fstab
$ mount /srv/salt
```

## Activation

Salt state modules (sls) need to be placed inside `top.sls`, e.g.

```
base:
  '*':
    - enable-atop
```