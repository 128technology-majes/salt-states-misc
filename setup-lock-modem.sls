# Install a lock_modem service

/usr/local/sbin/lock_modem.py:
  file.managed:
    - user: root
    - group: root
    - mode: 755
    - source: salt://files/lock_modem.py

systemd-reload-lock-modem-service:
  cmd.run:
   - name: systemctl --system daemon-reload
   - onchanges:
     - file: lock-modem-service

lock-modem-service:
  file.managed:
    - user: root
    - group: root
    - name: /usr/lib/systemd/system/lock_modem.service
    - contents: |
       [Unit]
       Description=A service to lock an UMTS/LTE modem for one hour

       [Service]
       ExecStart=/usr/local/sbin/lock_modem.py 3600
